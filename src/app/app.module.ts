import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { AboutComponentComponent } from './about-component/about-component.component';
import { ProductDetailsComponentComponent } from './product-details-component/product-details-component.component';
import {RouterModule} from "@angular/router";
import { MainComponentComponent } from './main-component/main-component.component';
import { ProdDetailsGuardGuard } from './prod-details-guard.guard';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    AboutComponentComponent,
    ProductDetailsComponentComponent,
    MainComponentComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
        {
          path:'products/:id',
          component:ProductDetailsComponentComponent,
          canActivate:[ProdDetailsGuardGuard]
        },
        {
          path:'products',
          component:AppComponent
        },
        {
          path:'',
          component:AboutComponentComponent
        }
      ])
    
  ],
  providers: [
    ProdDetailsGuardGuard
  ],
  bootstrap: [MainComponentComponent]
})
export class AppModule { }

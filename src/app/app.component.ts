import { Component } from '@angular/core';
import { ProductItems } from './product-items';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AngularL2Topgear4';
  companyName = 'Amazon';
  childData : ProductItems;
}

import { Injectable } from '@angular/core';
import { ProductItems } from './product-items';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProductList } from './product-list';

@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {
  public producatList;
  constructor(private refHttpClient:HttpClient){
     
  }
  addProductService=(name:string, quantity:number)=>{
    if (name && quantity) {
      this.refHttpClient.post(`http://localhost:3000/products`, new ProductItems(quantity,name)).subscribe(
        data => {
          console.log('POST Request is successful ', data);
        },
        error => {
          console.log('Error', error);
        }
      );
      
    }
    }  

  loadData():Observable<ProductList[]>{
    return this.refHttpClient.get<ProductList[]>('http://localhost:3000/products');
  }
}



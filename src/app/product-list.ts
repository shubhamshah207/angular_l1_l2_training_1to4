export interface ProductList {
    id:number;
    name:string;
    quantity:number;
}


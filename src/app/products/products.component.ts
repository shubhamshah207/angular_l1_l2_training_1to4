import { Component, Input, Output,EventEmitter } from '@angular/core';
import { ProductItems } from '../product-items';
import { ProductServiceService } from '../product-service.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent  {
  @Input() receivedCompany:string;
  @Output() objEventEmitter=new EventEmitter<ProductItems>();
  producatList;
  errorMsg;
  constructor(private refProductService : ProductServiceService) { 
    
  }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(){
    this.producatList=this.refProductService.loadData();
    this.producatList.subscribe(data => this.producatList = data,
      error => this.errorMsg = error);
  }

  addProduct=(name:string, quantity:number)=>{
    if (name && quantity) {
      this.refProductService.addProductService(name,quantity);
      this.objEventEmitter.emit(new ProductItems(quantity,name));
      alert("Product added successfully");
      
      this.loadData();
      
    }  
  }
  sendData = (product:ProductItems) => {
    
  }

}

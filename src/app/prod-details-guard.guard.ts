import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { ProductServiceService } from './product-service.service';

@Injectable({
  providedIn: 'root'
})
export class ProdDetailsGuardGuard implements CanActivate {
  constructor(private router:Router){

  }
  canActivate(
    next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      let bool=confirm("Are you sure you want to view the details?");
      if(bool) {
        return bool;
      }
      else {
        this.router.navigate(['/products']);
      }
      
  }
}

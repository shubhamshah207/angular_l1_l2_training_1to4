import { TestBed } from '@angular/core/testing';

import { ProdDetailsGuardGuard } from './prod-details-guard.guard';

describe('ProdDetailsGuardGuard', () => {
  let guard: ProdDetailsGuardGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ProdDetailsGuardGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
